"""
Create new connections to the database by passing connection details corresponding to
one of the db_auth_adapters.
"""

import psycopg2
from .db_auth_adapter import build_db_auth


class ConnectionFactory:
    def __init__(
        self, user, host, dbname, password, **kwargs
    ):  # pylint: disable=unused-argument
        self.user = user
        self.host = host
        self.dbname = dbname
        self.password = password

    def get_connection(self):
        return psycopg2.connect(
            user=self.user, host=self.host, dbname=self.dbname, password=self.password,
        )

    @classmethod
    def get_connection_factory(cls, dbname, **kwargs):
        db_auth = build_db_auth(kwargs)
        return ConnectionFactory(
            user=db_auth["user"],
            host=db_auth["host"],
            password=db_auth["password"],
            dbname=dbname,
        )
