"""
Create if not exists and then migrate a schema.
"""

import logging

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from yoyo import read_migrations
from yoyo import get_backend

logger = logging.getLogger("yoyo.migrations")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
logger.addHandler(handler)


class Migration:
    def __init__(
        self, user, host, default_db_name, password
    ):  # pylint: disable=too-many-arguments
        self.user = user
        self.host = host
        self.default_db_name = default_db_name
        self.password = password

    def create_connection(self):
        return psycopg2.connect(
            user=self.user,
            host=self.host,
            dbname=self.default_db_name,
            password=self.password,
            connect_timeout=3,
        )

    def create_if_not_exists(self, db_to_migrate):
        with self.create_connection() as conn:
            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            with conn.cursor() as cur:
                if not self.database_exists(cur, db_to_migrate):
                    cur.execute(f"CREATE Database {db_to_migrate};")

    def delete_if_exists(self, db_to_migrate):
        with self.create_connection() as conn:
            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            with conn.cursor() as cur:
                if self.database_exists(cur, db_to_migrate):
                    cur.execute(f"DROP Database {db_to_migrate};")

    def database_exists(self, cur, db_to_migrate):
        cur.execute(
                "SELECT count(*) FROM pg_database WHERE datname = %s;",
                (db_to_migrate,),
        )
        db_count = cur.fetchone()[0]
        return db_count != 0

    def run_migration(self, db_to_migrate):
        self.create_if_not_exists(db_to_migrate)
        print(
            "connecting to "
            f"postgres://{self.user}:<redacted>@{self.host}/{db_to_migrate}"
        )

        backend = get_backend(
            f"postgres://{self.user}:{self.password}@{self.host}/{db_to_migrate}"
        )

        migrations = read_migrations("./db_scripts")

        with backend.lock():
            to_apply = backend.to_apply(migrations)

            print(
                f"About to run the following {len(to_apply)} migrations: "
                f"{to_apply} \n to database {db_to_migrate}"
            )

            backend.apply_migrations(to_apply)

        print(f"Completed Migration for db {db_to_migrate}")

    def run_migrations(self, dbs_to_migrate):
        for db_to_migrate in dbs_to_migrate:
            self.run_migration(db_to_migrate)
