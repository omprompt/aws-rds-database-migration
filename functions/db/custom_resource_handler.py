""" Custom ressource handler to run all the db migrations. """

import logging
from functions.db.cfn_response import send, SUCCESS, FAILED
from functions.db.migration import Migration
from functions.db import get_db_auth_from_environment, DEFAULT_DB_NAME


def handle(event, context):
    try:
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        logger.info(event)
        if event["RequestType"] == "Delete":
            logger.info(
                "Marking custom as deleted, no data,"
                "nor databases have been deleted (no-op)"
            )
            send(event, context, SUCCESS)
            return
        db_auth = get_db_auth_from_environment()
        migration = Migration(**db_auth, default_db_name=DEFAULT_DB_NAME)
        tenant_db_name = event["ResourceProperties"]["TenantName"]
        logger.info("About to migrate: %s", str(tenant_db_name))
        migration.run_migration(tenant_db_name)
        send(event, context, SUCCESS)
    except Exception as err:  # pylint:disable=broad-except
        logger.error(f"An error occured: {err}")
        send(event, context, FAILED, reason=str(err))
