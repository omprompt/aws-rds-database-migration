"""
For interaction with the database.
"""
import os

from .db_auth_adapter import build_db_auth
from .connection_factory import ConnectionFactory

CACHED_DB_AUTH = None
DEFAULT_DB_NAME = "postgres"


def get_connection_factory_from_environment(dbname):
    db_auth = get_db_auth_from_environment()
    return ConnectionFactory(**db_auth, dbname=dbname)


def preprocess_environ(environ):
    return {
        strip_prefix(key).lower(): value
        for key, value in environ.items()
        if strip_prefix(key) is not None
    }


def strip_prefix(key):
    return key[3:] if key.startswith("IE_") else None


def get_db_auth_from_environment():
    global CACHED_DB_AUTH  # pylint: disable=global-statement
    if CACHED_DB_AUTH is None:
        env = preprocess_environ(os.environ)  # cleanse
        CACHED_DB_AUTH = build_db_auth(env)

    return CACHED_DB_AUTH


def clear_db_auth_cache():
    global CACHED_DB_AUTH  # pylint:disable=global-statement
    CACHED_DB_AUTH = None
