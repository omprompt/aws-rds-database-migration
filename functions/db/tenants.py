""" Transforming tenant names to useable db string names """

# Don't use space in tenant names
tenant_names = [""]


def get_db_name_for_tenant(tenant, stage):
    stripped_stage = stage.replace("-", "_")
    return f"{tenant}_{stripped_stage}".lower()


def get_tenant_db_names(stage):
    return [get_db_name_for_tenant(tenant, stage) for tenant in tenant_names]
