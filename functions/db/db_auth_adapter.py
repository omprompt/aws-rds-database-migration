"""
Adapters for getting the credentials required to get a db connection (user, password, host, dbname).
The intent is to allow a user of this library to provide the means to get the credentials and these
adapters will do the job of pulling the information together.

Eg/ the cloudformation adapter will retrieve a secret arn from a cloud formation
stack and then retrieve the secret from secrets manager.

This library is used to build the cli for migrations as well as called from app code.
"""

import json
from functions.aws import cloudformation, secretsmanager


class DBAuthAdapter:
    def is_applicable(self, args_dict):
        return all(args_dict.get(arg) is not None for arg in self.required_args())

    @classmethod
    def description(cls):
        pass

    @classmethod
    def required_args(cls):
        pass


class CloudFormationDBAuthAdapter(DBAuthAdapter):
    @classmethod
    def description(cls):
        return (
            "Cloudformation",
            "Retrieve the database connection "
            "details from a secret outputted from a cloudformation stack.",
        )

    @classmethod
    def required_args(cls):
        return {
            "stack_name": "The name of the cloudformation stack which output the secret id.",
            "secret_key": (
                "The key in the CF template output of the secretsManager "
                "secret_id containing the rds connection details."
            ),
        }

    def db_auth(self, args_dict):
        secret_id = cloudformation.get_stack_ouput(
            args_dict["stack_name"], args_dict["secret_key"]
        )
        return auth_from_secret(secret_id)


class SecretsManagerDBAuthAdapter(DBAuthAdapter):
    @classmethod
    def description(cls):
        return (
            "SecretsManager",
            "Retrieve the database connection details from a secret by id.",
        )

    @classmethod
    def required_args(cls):
        return {
            "secret_id": "The secretsManager secret_id containing the rds connection details."
        }

    def db_auth(self, args_dict):
        return auth_from_secret(args_dict["secret_id"])


def auth_from_secret(secret_id):
    secret_as_string = secretsmanager.get_secret(secret_id)
    secret = json.loads(secret_as_string)
    return {
        "user": secret["username"],
        "password": secret["password"],
        "host": secret["host"],
    }


class DBConnectionDetailsDBAuthAdapter(DBAuthAdapter):
    @classmethod
    def description(cls):
        return ("DBConnectionDetails", "Provide the database credentials as arguments.")

    @classmethod
    def required_args(cls):
        return {
            "user": "The username to use when creating database connection",
            "host": "The hostname of the database",
            "password": "The password for the database",
        }

    def db_auth(self, args_dict):
        return {
            "user": args_dict["user"],
            "password": args_dict["password"],
            "host": args_dict["host"],
        }


def get_db_auth_adapters():
    return [
        CloudFormationDBAuthAdapter(),
        SecretsManagerDBAuthAdapter(),
        DBConnectionDetailsDBAuthAdapter(),
    ]


def select_db_auth_adapter(conn_args):
    for adapter in get_db_auth_adapters():
        if adapter.is_applicable(conn_args):
            return adapter

    return None


def build_db_auth(conn_args):
    adapter = select_db_auth_adapter(conn_args)
    if adapter is None:
        return None
    return adapter.db_auth(conn_args)
