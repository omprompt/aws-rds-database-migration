"""Simple interface for interaction with AWS Cloudformation"""
import boto3
from botocore.exceptions import ClientError
from functions.aws import s3, fetch_paginated

CF_RESOURCE = boto3.resource("cloudformation")
CF_CLIENT = boto3.client("cloudformation")
boto3.set_stream_logger("boto3.resources")


def fetch_stacks():
    return CF_RESOURCE.stacks.all()  # pylint: disable=maybe-no-member


def fetch_all_resources(stack_name):
    pages = fetch_paginated.fetch(CF_CLIENT.list_stack_resources, StackName=stack_name)
    return [
        stackResourceSummary
        for page in pages
        for stackResourceSummary in page["StackResourceSummaries"]
    ]


def buckets_in_stack(stack_name):
    stack_resource_summaries = fetch_all_resources(stack_name)

    buckets_to_empty = [
        stack_resource_summary
        for stack_resource_summary in stack_resource_summaries
        if stack_resource_summary["ResourceType"] == "AWS::S3::Bucket"
        and stack_resource_summary["ResourceStatus"] != "DELETE_COMPLETE"
    ]

    return buckets_to_empty


def empty_buckets_in_stack(stack_name):
    buckets_to_empty = buckets_in_stack(stack_name)
    for bucket in buckets_to_empty:
        print(bucket)
        s3.empty_bucket(bucket["PhysicalResourceId"])


def stack_exists(stack_name):
    try:
        CF_CLIENT.describe_stacks(StackName=stack_name)
        return True
    except ClientError as error:
        if error.response["Error"]["Code"] == "ValidationError":
            print("Stack {n} does not exist".format(n=stack_name))
        else:
            print(f"Unexpected error: {error}")
        return False


def delete_stack(stack_name, wait=True, empty_buckets=False):
    print("deleting", stack_name)
    if stack_exists(stack_name):
        if empty_buckets:
            empty_buckets_in_stack(stack_name)
        CF_CLIENT.delete_stack(StackName=stack_name)
        print("Deletion has started for stack", stack_name)
        if wait:
            waiter = CF_CLIENT.get_waiter("stack_delete_complete")
            waiter.wait(
                StackName=stack_name, WaiterConfig={"Delay": 10, "MaxAttempts": 30}
            )
            print("Deletion complete for stack", stack_name)


def get_stack_ouput(cf_stack_name, output_key):
    stack = CF_RESOURCE.Stack(cf_stack_name)

    output = next(
        (output for output in stack.outputs if output["OutputKey"] == output_key), None,
    )

    if output is None:
        raise Exception(
            "Could not find output in stack with name "
            f"{cf_stack_name} with OutputKey: {output_key}.\n\n"
            f"Query for stack returned: {stack}.\n"
            f"Outputs were: {stack.outputs}."
        )
    print(
        f"Found output {output} in stack with name {cf_stack_name} with OutputKey: {output_key}. "
    )
    return output["OutputValue"]
