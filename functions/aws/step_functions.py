"""Simple interface for interaction with AWS Step Functions"""


import boto3

CLIENT = boto3.client("stepfunctions")


def start_execution(state_machine_arn, name, message):
    """
    Invoke A Step Function

    Parameters:
    -----------
    :param state_machine_arn: Identification of the aws step function resource

    :param name: Step function id

    :param message: JSON message. Used as input
    """
    return CLIENT.start_execution(
        stateMachineArn=state_machine_arn, name=name, input=message
    )


def get_execution_history(execution_arn, reverse_order=True):
    return CLIENT.get_execution_history(
        executionArn=execution_arn, reverseOrder=reverse_order
    )


def describe_execution(execution_arn):
    return CLIENT.describe_execution(executionArn=execution_arn)


def get_num_of_executions(state_machine_arn, status):
    paginator = CLIENT.get_paginator("list_executions")
    page_iterator = paginator.paginate(
        stateMachineArn=state_machine_arn, statusFilter=status
    )
    return sum(len(page["executions"]) for page in page_iterator)
