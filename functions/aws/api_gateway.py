"""Interface for interacting with API gateway """

from aws_requests_auth.boto_utils import BotoAWSRequestsAuth

def build_iam_auth(host, region, service):
    return BotoAWSRequestsAuth(
        aws_host=host,
        aws_region=region,
        aws_service=service
    )

def iam_auth_from_endpoint(endpoint):
    split_url = endpoint.split(".")
    return build_iam_auth(
        endpoint.split("/")[2],
        split_url[2],
        split_url[1]
    )
