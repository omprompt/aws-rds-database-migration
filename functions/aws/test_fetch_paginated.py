import unittest
from . import fetch_paginated

# pylint: skip-file


def mock_boto3_function(NextToken="DefaultToken", StackName=""):
    if StackName == "multiPageStack123":
        token_to_response = {
            "DefaultToken": {"Data": {}, "NextToken": "Token1"},
            "Token1": {"Data": {}, "NextToken": "Token2"},
            "Token2": {"Data": {}},
        }
        return token_to_response.get(NextToken)
    if StackName == "singlePageStack123":
        return {"Data": {"a": 1}}
    raise Exception("no valid stack name provided", StackName)


class TestFetchPaginated(unittest.TestCase):
    def test_get_single_page(self):
        result = fetch_paginated.fetch(
            mock_boto3_function, StackName="singlePageStack123"
        )
        self.assertEqual(result, [{"Data": {"a": 1}}])

    def test_get_multiple_page(self):
        result = fetch_paginated.fetch(
            mock_boto3_function, StackName="multiPageStack123"
        )
        self.assertEqual(
            result,
            [
                {"Data": {}, "NextToken": "Token1"},
                {"Data": {}, "NextToken": "Token2"},
                {"Data": {}},
            ],
        )
