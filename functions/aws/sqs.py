"""Simple interface for interaction with SQS"""

import json
import boto3

CLIENT = boto3.client(service_name="sqs")


def get_messages_from_queue_by_url(url, **kwargs):
    return CLIENT.receive_message(QueueUrl=url, **kwargs)


def delete_message_from_queue(url, receipt_handle):
    return CLIENT.delete_message(QueueUrl=url, ReceiptHandle=receipt_handle)


def send_message(url, message_body, **kwargs):
    return CLIENT.send_message(QueueUrl=url, MessageBody=message_body, **kwargs)


def send_dict(url, message_body, **kwargs):
    assert isinstance(message_body, dict)
    return send_message(url, json.dumps(message_body), **kwargs)


def get_one_message_from_queue_or_none(queue_url):
    resp = get_messages_from_queue_by_url(queue_url, MaxNumberOfMessages=1)
    if "Messages" not in resp:
        return None
    return resp["Messages"][0]


class SQSQueueReader:
    """
    A simplified interface for dealing with SQS one message at a time.

    Message bodies are assumed to be json.

    get_one_message will return the parsed message_body (as a dict)
    and the receipt handle.

    delete_message takes the reciept handle and will delete from the queue.
    """

    def __init__(self, queue_url):
        self.queue_url = queue_url

    def get_one_message(self):
        message = get_one_message_from_queue_or_none(self.queue_url)
        if message is None:
            return None, None
        message_body = json.loads(message["Body"])
        reciept_handle = message["ReceiptHandle"]
        return message_body, reciept_handle

    def delete_message(self, receipt_handle):
        return delete_message_from_queue(self.queue_url, receipt_handle)
