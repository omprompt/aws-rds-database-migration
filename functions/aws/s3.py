"""Simple interface for interaction with AWS S3"""

import json

import boto3

# pylint: disable=no-member
S3 = boto3.resource("s3")
s3_client = boto3.client("s3")


def get_file_as_stream(bucket, key):
    obj = S3.Object(bucket, key)
    return obj.get()["Body"]


def get_file(bucket, key):
    """Download a file from S3 and return the contents as a string"""
    return get_file_as_stream(bucket, key).read().decode("UTF-8")


def load_dict(bucket, key):
    """Download a json file from S3 as a dict"""
    return json.loads(get_file(bucket, key))


def load_json_if_present(bucket, key):
    """Download a file from S3 as a dict if it exists, else return None"""
    try:
        return load_dict(bucket, key)
    except Exception as error:  # pylint:disable=W0703 # QUESTION - what exception type? #
        print(f"Error was thrown loading json from {key}")
        print(error)
        return None


def load_list_if_present(bucket, key):
    list_file = load_json_if_present(bucket, key)
    return list_file if isinstance(list_file, list) else None


def put_string(bucket, key, data):
    """
    Save an object to the specified S3 location
    Encodes as utf-8
    """
    obj = S3.Object(bucket, key)
    obj.put(Body=data.encode("utf-8"))


def put_json(bucket, key, data):
    """
    Save an object to the specified S3 location
    Calls json.dump on the object to stringify it for saving
    """
    obj = S3.Object(bucket, key)
    obj.put(Body=json.dumps(data))


def put_object(bucket, key, file_name):
    """
    Save an object to the specified S3 location
    """
    obj = S3.Object(bucket, key)
    obj.upload_file(file_name)


def empty_bucket(bucket_name):
    bucket = S3.Bucket(bucket_name)
    print("Emptying bucket-->", bucket)
    if bucket:
        bucket.objects.all().delete()


def list_objects(bucket, prefix):
    return s3_client.list_objects_v2(Bucket=bucket, Prefix=prefix)


class S3Bucket:  # pylint: disable=unused-argument
    def __init__(self, bucket_name):
        self.bucket_name = bucket_name

    def put_json(self, key=None, contents=None, **metadata):
        put_json(self.bucket_name, key, contents)

    def load_dict(self, key=None, **metadata):
        return load_dict(self.bucket_name, key)

    def get_file_as_stream(self, key=None, **metadata):
        return get_file_as_stream(self.bucket_name, key)

    def put_string(self, key=None, contents=None, **metadata):
        return put_string(self.bucket_name, key, contents)
