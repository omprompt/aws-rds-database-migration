"""Helper for interacting with paginated AWS services."""


def fetch(func, **kwargs):
    """Retrieve paginated results which match the AWS pagination format.
        {NextToken: '', ... }
        Stops when NextToken is not returned.
    """
    response = func(**kwargs)
    result = [response]
    while "NextToken" in response:
        response = func(NextToken=response["NextToken"], **kwargs)
        result.append(response)
    return result
