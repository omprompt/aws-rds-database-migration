"""Simple interface for interaction with AWS SNS"""

import boto3

CLIENT = boto3.client("sns")


def send_message_with_attributes(topic_arn, payload, attributes):
    return CLIENT.publish(
        TopicArn=topic_arn, Message=payload, MessageAttributes=attributes
    )


def send_message(topic_arn, payload):
    return CLIENT.publish(TopicArn=topic_arn, Message=payload)
