"""Simple interface for interaction with AWS Textract"""

import boto3
from botocore.exceptions import ClientError

from functions.utilities.exceptions import RateLimitException, TextractRejectedException
from . import fetch_paginated

CLIENT = boto3.client("textract")


def flatten_textract_response(pages):
    blocks = [block for page in pages for block in page["Blocks"]]
    return {
        "DocumentMetadata": pages[0]["DocumentMetadata"],
        "JobStatus": pages[0]["JobStatus"],
        "Blocks": blocks,
    }


def get_document_analysis_for_job(job_id):
    pages = fetch_paginated.fetch(CLIENT.get_document_analysis, JobId=job_id)
    return flatten_textract_response(pages)


class TextractInvoker:
    """
    An interface to allow simplified invocation of textract.
    """

    def __init__(self, notification_role_arn, sns_topic_arn):
        self.notification_role_arn = notification_role_arn
        self.sns_topic_arn = sns_topic_arn

    def invoke_textract(self, bucket, key, job_tag, client_request_token):
        """
        A simple wrapper around start document analysis.

        Returns a 2ple (job_id, error) with error None if successful
        and job_id None if an error occurred.

        In the case of rate limiting a RateLimitException is
        exchanged for the raw textract Error.
        """

        try:
            job_id = CLIENT.start_document_analysis(
                **self.get_invoke_params(bucket, key, job_tag, client_request_token)
            )
            return job_id, None
        except ClientError as err:
            if err.response["Error"]["Code"] == "LimitExceededException":
                return (None, RateLimitException())
            if err.response["Error"]["Code"] in [
                "BadDocumentException",
                "DocumentTooLargeException",
                "UnsupportedDocumentException",
            ]:
                return (None, TextractRejectedException())
            return (None, err)
        except Exception as err:  # pylint: disable=W0703
            return (None, err)

    def get_invoke_params(self, bucket, key, job_tag, client_request_token):
        return {
            "ClientRequestToken": client_request_token,
            "DocumentLocation": {"S3Object": {"Bucket": bucket, "Name": key,},},
            "FeatureTypes": ["TABLES", "FORMS",],
            "NotificationChannel": {
                "RoleArn": self.notification_role_arn,
                "SNSTopicArn": self.sns_topic_arn,
            },
            "JobTag": job_tag,
        }
