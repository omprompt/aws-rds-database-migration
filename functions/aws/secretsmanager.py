"""
Get secrets from the secrets manager.

Ids of secrets should be parameterised in environment variables
as they are global as so you need different ids per environment.
"""

import boto3

SECRETS_CLIENT = boto3.client("secretsmanager")


def get_secret(secret_id, key="SecretString"):
    """
    secret_id - string - can be either the ARN or the friendly name of the secret.
    """
    resp = SECRETS_CLIENT.get_secret_value(SecretId=secret_id)
    if key not in resp:
        raise Exception(
            f"No key named '{key}' returned for secret with name {secret_id}. "
            f"Returned response {resp}"
        )
    return resp[key]
