"""Simple interface for interaction with AWS Textract"""

import json

import botocore.config
import boto3

from . import fetch_paginated


conf = botocore.config.Config(
    retries={"max_attempts": 10, "mode": "adaptive"}
)


CLIENT = boto3.client("textract", config=conf)


MAX_POLLING_TIME = 180
POLLING_INTERVAL = 10


class TextractFailedException(Exception):
    """
    Raised when Textract can't process a document and never will,
    such as an ivalid file format or a document that is too large
    """


def flatten_textract_response(pages):
    blocks = [block for page in pages for block in page.get("Blocks", [])]
    return {
        "DocumentMetadata": pages[0].get("DocumentMetadata"),
        "JobStatus": pages[0]["JobStatus"],
        "Blocks": blocks,
    }


def get_document_analysis_for_job(job_id):
    pages = fetch_paginated.fetch(CLIENT.get_document_analysis, JobId=job_id)
    return flatten_textract_response(pages)


def poll_for_textract_result(sleep_function, job_id):
    for _ in range(0, MAX_POLLING_TIME // POLLING_INTERVAL):
        # We notw pass a client config to do exponential backoff
        raw = get_document_analysis_for_job(job_id)
        if raw["JobStatus"] != "IN_PROGRESS":
            break
        sleep_function(POLLING_INTERVAL)
    else:
        raise TimeoutError("Did not get response from textract in time")
    if raw["JobStatus"]  not in ("SUCCEEDED", "PARTIAL_SUCCESS"):
        raise TextractFailedException(json.dumps(raw))
    return raw


def invoke_simple(bucket, key, job_tag):
    client_request_token = "".join(
        [char for char in job_tag if char.isalnum()]
    )
    return CLIENT.start_document_analysis(
        ClientRequestToken=client_request_token,
        DocumentLocation={"S3Object": {"Bucket": bucket, "Name": key,},},
        FeatureTypes=["TABLES", "FORMS"],
        JobTag=job_tag,
    )
