"""
Set up the python layers directories by adding them to the path like virtual environments.
"""

import glob
import os
import platform
import sys
import logging

PY_VER = ".".join(platform.python_version_tuple()[:2])

CURRENT_DIR = os.path.dirname(os.path.dirname(__file__))
file_list = glob.glob("requirements/layers/*/requirements.txt")
for filename in file_list:
    folder = os.path.dirname(filename)
    path_folder = os.path.join(
        CURRENT_DIR, folder, f"python/lib/python{PY_VER}/site-packages"
    )
    sys.path.append(path_folder)
logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)
