cd $(dirname $BASH_SOURCE)

ENVIRONMENT=$1

if [ -z "$ENVIRONMENT" ]
then
    echo "no environment provided"
    exit 1
fi

../node_modules/.bin/sls deploy -v --config ./serverless-migration.yml -s $ENVIRONMENT